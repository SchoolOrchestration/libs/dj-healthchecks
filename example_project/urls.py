from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from rest_framework import routers
from healthchecks.api import HealthViewSet

router = routers.DefaultRouter()
router.register(r'health', HealthViewSet, basename='health')

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^', include(router.urls)),
]
